package com.example.AC;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Switch;

import androidx.appcompat.app.AppCompatActivity;

public class kaiguan2 extends AppCompatActivity {

    ImageButton Ret;
    ImageButton Button1;
    ImageButton Button2,Button3;
    Switch swh;
    Button more1;
    Button more2;

    //跳转下一页面时，不销毁当前页面
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK){
            moveTaskToBack(true);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.kaiguan2);
        //去掉标题栏
        this.getSupportActionBar().hide();


        Button1 = (ImageButton) findViewById(R.id.but_1);
        Button1.setOnClickListener(new View.OnClickListener() {
            int count = 0;
            public void onClick(View v) {
                count += 1;
                if (count == 1) {
                    Button1.setBackgroundResource(R.drawable.a);
                } else if (count == 2) {
                    count = 0;
                    Button1.setBackgroundResource(R.drawable.b);
                }
            }
        });

        swh = (Switch) findViewById(R.id.swh_status);
        Button2 = (ImageButton) findViewById(R.id.btnIncrease);
        Button3 = (ImageButton) findViewById(R.id.btnDecrease);
        more1 = (Button) findViewById(R.id.more1);
        more2 = (Button) findViewById(R.id.more2);
        swh.setOnClickListener(new View.OnClickListener() {
            int count = 0;
            public void onClick(View v) {
                count += 1;
                if (count == 1) {
                    Button1.setBackgroundResource(R.drawable.a);
                    Button2.setBackgroundResource(R.drawable.jia2);
                    Button3.setBackgroundResource(R.drawable.jian2);
                    more1.setBackgroundColor(Color.parseColor("#FF05910e"));
                    more2.setBackgroundColor(Color.parseColor("#FF05910e"));
                } else if (count == 2) {
                    count = 0;
                    Button1.setBackgroundResource(R.drawable.b);
                    Button2.setBackgroundResource(R.drawable.jia1);
                    Button3.setBackgroundResource(R.drawable.jian1);
                    more1.setBackgroundColor(Color.parseColor("#FF8a8a8a"));
                    more2.setBackgroundColor(Color.parseColor("#FF8a8a8a"));
                }
            }
        });

        Ret = findViewById(R.id.ret);
        Ret.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(kaiguan2.this, MainActivity.class);
                startActivity(intent);

            }
        });

        more1 = findViewById(R.id.more1);
        more1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(kaiguan2.this, fengsu2.class);
                startActivity(intent);

            }
        });

        more2 = findViewById(R.id.more2);
        more2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(kaiguan2.this, moshi2.class);
                startActivity(intent);

            }
        });

    }
}
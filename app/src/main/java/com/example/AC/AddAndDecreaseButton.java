package com.example.AC;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class AddAndDecreaseButton extends LinearLayout
{
    private int amount = 25;     //初始温度

    private TextView etAmount;
    private ImageButton btnDecrease;
    private ImageButton btnIncrease;

    @SuppressLint("WrongViewCast")
    public AddAndDecreaseButton(Context context, AttributeSet attrs)
    {
        super(context, attrs);

        LayoutInflater.from(context).inflate(R.layout.wendu, this);

        etAmount = findViewById(R.id.etAmount);
        btnDecrease = findViewById(R.id.btnDecrease);
        btnIncrease = findViewById(R.id.btnIncrease);
        etAmount.setText((amount) + "\u2103");

        //设置‘-’号按键点击事件
        btnDecrease.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (amount > 16)
                    amount--;
                else
                    Toast.makeText(getContext(), "已是最低温度", Toast.LENGTH_SHORT).show();
                etAmount.setText((amount) + "\u2103");

            }
        });

        //设置‘+’号按键点击事件
        btnIncrease.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (amount < 30)
                    amount++;
                else
                    Toast.makeText(getContext(), "已是最高温度", Toast.LENGTH_SHORT).show();
                etAmount.setText((amount) + "\u2103");
            }
        });
    }
}

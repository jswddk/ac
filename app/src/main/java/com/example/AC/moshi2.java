package com.example.AC;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageButton;

import androidx.appcompat.app.AppCompatActivity;

public class moshi2 extends AppCompatActivity implements View.OnClickListener {

    ImageButton Ret;
    ImageButton but_1,but_2,but_3,but_4,but_5,but_6;

    //跳转下一页面时，不销毁当前页面
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK){
            moveTaskToBack(true);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.moshi2);
        //去掉标题栏
        this.getSupportActionBar().hide();

        but_1 = (ImageButton) findViewById(R.id.but_1);
        but_2 = (ImageButton) findViewById(R.id.but_2);
        but_3 = (ImageButton) findViewById(R.id.but_3);
        but_4 = (ImageButton) findViewById(R.id.but_4);
        but_5 = (ImageButton) findViewById(R.id.but_5);
        but_6 = (ImageButton) findViewById(R.id.but_6);
        but_1.setOnClickListener(this);
        but_2.setOnClickListener(this);
        but_3.setOnClickListener(this);
        but_4.setOnClickListener(this);
        but_5.setOnClickListener(this);
        but_6.setOnClickListener(this);

        Ret = findViewById(R.id.ret);
        Ret.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(moshi2.this, kaiguan1.class);
                startActivity(intent);

            }
        });
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.but_1){//如果是but_1按钮，则设置一种背景
            but_1.setBackgroundResource(R.drawable.zidong1);
            but_2.setBackgroundResource(R.drawable.zhileng2);
            but_3.setBackgroundResource(R.drawable.zhire2);
            but_4.setBackgroundResource(R.drawable.chushi2);
            but_5.setBackgroundResource(R.drawable.shaofeng2);
            but_6.setBackgroundResource(R.drawable.sleep2);
        }else if(v.getId()==R.id.but_2){//如果是but_2按钮，则设置一种背景
            but_1.setBackgroundResource(R.drawable.zidong2);
            but_2.setBackgroundResource(R.drawable.zhileng1);
            but_3.setBackgroundResource(R.drawable.zhire2);
            but_4.setBackgroundResource(R.drawable.chushi2);
            but_5.setBackgroundResource(R.drawable.shaofeng2);
            but_6.setBackgroundResource(R.drawable.sleep2);
        }else if(v.getId()==R.id.but_3){//如果是but_3按钮，则设置一种背景
            but_1.setBackgroundResource(R.drawable.zidong2);
            but_2.setBackgroundResource(R.drawable.zhileng2);
            but_3.setBackgroundResource(R.drawable.zhire1);
            but_4.setBackgroundResource(R.drawable.chushi2);
            but_5.setBackgroundResource(R.drawable.shaofeng2);
            but_6.setBackgroundResource(R.drawable.sleep2);
        }else if(v.getId()==R.id.but_4){//如果是but_4按钮，则设置一种背景
            but_1.setBackgroundResource(R.drawable.zidong2);
            but_2.setBackgroundResource(R.drawable.zhileng2);
            but_3.setBackgroundResource(R.drawable.zhire2);
            but_4.setBackgroundResource(R.drawable.chushi1);
            but_5.setBackgroundResource(R.drawable.shaofeng2);
            but_6.setBackgroundResource(R.drawable.sleep2);
        }else if(v.getId()==R.id.but_5){//如果是but_5按钮，则设置一种背景
            but_1.setBackgroundResource(R.drawable.zidong2);
            but_2.setBackgroundResource(R.drawable.zhileng2);
            but_3.setBackgroundResource(R.drawable.zhire2);
            but_4.setBackgroundResource(R.drawable.chushi2);
            but_5.setBackgroundResource(R.drawable.shaofeng1);
            but_6.setBackgroundResource(R.drawable.sleep2);
        }else if(v.getId()==R.id.but_6){//如果是but_6按钮，则设置一种背景
            but_1.setBackgroundResource(R.drawable.zidong2);
            but_2.setBackgroundResource(R.drawable.zhileng2);
            but_3.setBackgroundResource(R.drawable.zhire2);
            but_4.setBackgroundResource(R.drawable.chushi2);
            but_5.setBackgroundResource(R.drawable.shaofeng2);
            but_6.setBackgroundResource(R.drawable.sleep1);
        }
    }
}

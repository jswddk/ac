package com.example.AC;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import androidx.appcompat.app.AppCompatActivity;


public class MainActivity extends AppCompatActivity{
    ImageButton Btn1,Btn2;
    Button Btn3;

    //跳转下一页面时，不销毁当前页面
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK){
            moveTaskToBack(true);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //去掉标题栏
        this.getSupportActionBar().hide();

        Btn1=findViewById(R.id.btn_1);
        Btn1.setOnClickListener(new View.OnClickListener() {
            int count=0;
            public void onClick(View v){
                count+=1;
                if(count==1){
                    Btn1.setBackgroundResource(R.drawable.a);
                    Intent intent=new Intent(MainActivity.this,kaiguan1.class);
                    startActivity(intent);
                }else if(count==2){
                    count=0;
                    Btn1.setBackgroundResource(R.drawable.b);
                }
            }
        });

        Btn2=findViewById(R.id.btn_2);
        Btn2.setOnClickListener(new View.OnClickListener() {
            int count=0;
            public void onClick(View v){
                count+=1;
                if(count==1){
                    Btn2.setBackgroundResource(R.drawable.a);
                    Intent intent=new Intent(MainActivity.this,kaiguan2.class);
                    startActivity(intent);
                }else if(count==2){
                    count=0;
                    Btn2.setBackgroundResource(R.drawable.b);
                }
            }
        });

        Btn3 = (Button)findViewById(R.id.btn_3);
        Btn3.setOnClickListener(new View.OnClickListener(){
            int count=0;
            public void onClick(View v){
                count+=1;
                if(count==1){
                    Btn1.setBackgroundResource(R.drawable.a);
                    Btn2.setBackgroundResource(R.drawable.a);
                }else if(count==2){
                    count=0;
                    Btn1.setBackgroundResource(R.drawable.b);
                    Btn2.setBackgroundResource(R.drawable.b);
                }
            }
        });

    }
}

package com.example.AC;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageButton;

import androidx.appcompat.app.AppCompatActivity;

public class fengsu2 extends AppCompatActivity implements View.OnClickListener {

    ImageButton Ret;
    ImageButton but_1,but_2,but_3,but_4;

    //跳转下一页面时，不销毁当前页面
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK){
            moveTaskToBack(true);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fengsu2);
        //去掉标题栏
        this.getSupportActionBar().hide();

        but_1 = (ImageButton) findViewById(R.id.but_1);
        but_2 = (ImageButton) findViewById(R.id.but_2);
        but_3 = (ImageButton) findViewById(R.id.but_3);
        but_4 = (ImageButton) findViewById(R.id.but_4);
        but_1.setOnClickListener(this);
        but_2.setOnClickListener(this);
        but_3.setOnClickListener(this);
        but_4.setOnClickListener(this);

        Ret = findViewById(R.id.ret);
        Ret.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(fengsu2.this, kaiguan1.class);
                startActivity(intent);

            }
        });

    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.but_1){  //如果是but_1按钮，则设置另一种背景
            but_1.setBackgroundResource(R.drawable.zidong1);
            but_2.setBackgroundResource(R.drawable.one2);
            but_3.setBackgroundResource(R.drawable.two2);
            but_4.setBackgroundResource(R.drawable.three2);
        }else if(v.getId()==R.id.but_2){    //如果是but_2按钮，则设置另一种背景
            but_1.setBackgroundResource(R.drawable.zidong2);
            but_2.setBackgroundResource(R.drawable.one1);
            but_3.setBackgroundResource(R.drawable.two2);
            but_4.setBackgroundResource(R.drawable.three2);
        }else if(v.getId()==R.id.but_3){    //如果是but_3按钮，则设置另一种背景
            but_1.setBackgroundResource(R.drawable.zidong2);
            but_2.setBackgroundResource(R.drawable.one2);
            but_3.setBackgroundResource(R.drawable.two1);
            but_4.setBackgroundResource(R.drawable.three2);
        }else if(v.getId()==R.id.but_4){    //如果是but_4按钮，则设置另一种背景
            but_1.setBackgroundResource(R.drawable.zidong2);
            but_2.setBackgroundResource(R.drawable.one2);
            but_3.setBackgroundResource(R.drawable.two2);
            but_4.setBackgroundResource(R.drawable.three1);
        }
    }
}